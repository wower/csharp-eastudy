# A Comparison of Genetic Algorithms and Simulated Annealing Applied to Discrete Design Problems

Blair Birdsell   
[linkedin.com/in/blairbirdsell](linkedin.com/in/blairbirdsell)  
December 13 2021  

<img src="./readme-images/Project-Banner.png" width=400>

# Abstract
This paper addresses the growing need in the AEC industry to optimize building problems with discrete characteristics. These are the types of problems come up when trying to size mechanical systems or facade components, and have a direct impact on the efficiency of prefabricated and modular construction techniques such as mass-timber. Two widely available optimization algorithms implemented in commercial software are considered, simulated annealing and genetic algorithms. The project attempts to answer and add context around how long it should take to find an approximately optional design solution from these two options.

Results are presented from two comparisons, one a well-known computer science problem, the other an example from the AEC industry. The paper describes the implementation of each algorithm and analyzes their computational efficiency and convergence properties. The conclusions highlight that while genetic algorithms were able to deliver better design options, they were not significantly better. Simulated annealing, on the other hand, was consistently faster at finding high-value design options.

# Keywords
Optimization. Modelling and Simulation. Building Performance. Genetic Algorithm. Simulated Annealing. Machine Learning.

# 1 Introduction
With the increased implementations of optimization algorithms in commercial software, and the increasing adoption of prefabricated and modular building techniques, a knowledge gap has emerged in the architectural, engineering, and construction industry in regards to the discrete optimization of building components. Owning to the complexity of the subject matter, it's not often straightforward to know if a certain algorithm is appropriate for an application or whether the outputted results are reasonable and correct. 

Given the high capital costs of building and long-term impact of a building's energy performance, the effect of optimization on a project's costs and lifecycle can be substantial. However, optimization in the AEC industry is a large subject, and not all methods can be addressed in detail in this paper. It was instead decided to focus on two different discrete optimization algorithms which are widely available and currently in use in the architecture and engineering industry. This allows for a more in-depth analysis of their computational efficiency and convergence properties. Implementations of both simulated annealing and genetic algorithms are currently available in different types of commercial software,[^1] and have a track record of use in space planning, structural optimization, facade design, and building energy optimization (Li Li 2012) (Evins 2013). 

# 2 Optimization

## 2.1 Why Optimize
As sustainability's influence on building design continues to grow, subtle and complex questions arise about the application of optimization in a project which need to be answered. Much grief could be saved if every project was over-engineered without regard to costs. However, industry has a responsibility to produce materially efficient and sustainable structures for clients and society (Debney 2020). 

To briefly describe some of the factors which influence building optimization problems, the first question one faces is whether the problem is single-objective or multi-objective. Most building projects have multiple conflicting goals which must be integrated and balanced to find an optimal design solution (Wortmann 2020)(Evins 2013). Both experiments in this project use a cost-function that tries to optimize only one variable. 

It is also important to recognize the interplay between discrete and continuous parameters in building design problems. Continuous optimization problems tend to have robust numerical solvers available which take advantage of the fitness landscape's gradient to efficiently find the global minima or maxima. Discrete problems, on the other hand, do not have such straightforward mappings. Therefore, when faced with a problem that has discrete or combinatorial elements, other heuristic-based methods should to be considered (Higham et al. 2015). In practice, building optimization problems tend to be hybrid of continuous and discrete variables. 

## 2.2 About the Algorithms 
The algorithms chosen for this paper are classified as heuristic methods, and follow general rules and short cuts implemented computationally to find an approximate solution rather than following a deterministic mathematical process to find an exact solution. This class of algorithms do not have proven convergence properties but still offer robust methods in practice to approximate optimal solutions (Evins 2013). 

Genetic algorithms should perhaps be called "evolutionary algorithms" because they share common inspiration from evolutionary principals such as mutation, selection, and inheritance (Goldberg 1989)(Rutten 2013). 

Simulated Annealing takes as its model thermodynamics and the cooling of crystalline structures. At the start of the process when the temperature is "high", the algorithm has a higher probability of jumping across the entire phase space. As the solver iterates through possible design solutions and "cools", the probability shifts toward a higher chance of retaining the best solution found so far (Kirkpatrick 1983) (Evins 2013). 

Wiabel 2018 draws our attention to some of the similarities and differences between these two approaches that allow the algorithms to find high-value design options in large combinatorial design spaces. The move operator in a GA applies mutation and cross-over to the previous generation to find new candidates. In SA it is the energy of the temperature parameter which acts as a changing step size tolerance to control how far from the previous candidate the algorithm will search for improvement. The selection operator refers to the method an algorithm uses to choose which candidates are saved for future reference and modification. SA only considers a population of one throughout its search of the design space and keeps track of only the best candidate. GA use a population of candidate solutions greater than one. This modification to the selection operator is a benefit in terms robustness at the expense of computational performance. Throughout this paper, robustness is used to describe the ability of an algorithm to find the global minima or maxima in the face of challenging features of the fitness landscape.

# 3 Traveling Salesmen Problem

## 3.1 Description 
The Travelling Salesmen Problem is a well-known test for the computational efficiency of algorithms in combinatorics. Exact solutions can be found recursively, but due to the problem's factorial complexity it might stress a computer's memory or time limits. This makes it a practical test bed for heuristic approaches (Higham et al. 2015). The problem's discrete structure can be found across many different disciplines including many applications in the building industry. This includes building problems where elements are modular (categorical), like mechanical units, or change in discrete steps, such as facades and structural components.

The problem consists of a series of cities, in our problem taken to be conceptual, and asks us to find the shortest tour of all cities returning to the original starting point. The cost function is evaluated as the length of travel with minimal being better. 

The search and timing functions were coded in Wolfram Mathematica 12.3 according to the references cited in section 2.2 and is included in this project's footnotes.[^2] Sixteen cities presents a solution space of approximately 2.092 * 10^13 combinations. (The built-in Mathematica function `FindShortestTour`, which implements a custom search algorithm behind the scenes to find exact solutions, is included for reference.[^3])

## 3.2 Results

### 3.2.1 Simulated Annealing vs Genetic Algorithm  
The experiment ran each function for 500 iterations and displays the short resulting tour. The GA using a population of 32 was significantly slower than SA but did return significantly better results compared to the actual solution. 

<img src="./readme-images/mm/mm-TSP-onerun-no-modifer.png" width=700>

*Table 1*

### 3.2.2 Modified Simulated Annealing vs Genetic Algorithm
To ensure a robust understanding of the results, a variation of above was run which modified the SA to run ten times more iterations over the GA. The results were much closer in terms of cost with SA sometimes beating GA. In all cases SA was still consistently faster. 

<img src="./readme-images/mm/mm-TSP-onerun-modifed.png" width=700>

*Table 2*

### 3.2.3 Direct comparison for same number of iterations. 
Finally averaging multiple runs was attempted with 20 cities representing approximately 2.43 * 10^18 possible combinations. Averaging over 10 runs with 1500 iterations, Table 3 highlights the performance characteristics of each algorithm. The GA consistently showed an improvement over SA in finding a shorter tour but SA was significantly faster across all 10 tests.

<img src="./readme-images/mm/mm-TSP-comparison1.png" width=700>

*Table 3*

# 4 Mass-Timber Optimization

## 4.1 Design Template

<img src="./readme-images/Tower-example.png" width=700>

*Figure 1. Tianfu Observation Tower 2020 - Chengdu China (StructureCraft and SKF)*

Expanding on the comparison above, we now turn to a more practical application of SA and GA. The Tianfu Observation Tower was completed in 2020, parts of which were designed using a genetic algorithm.[^4] The important characteristic here in terms of sustainable design is that the goal of utilizing computation in the project was to produce an evocative structure and reduce waste.[^5] [^6] The size of the mid-tier deck is approximately wide 6 meters and 4 meters deep.

For simplicity, this study does not include the optimization of the engineering calculations which were originally part of the Tianfu project. Instead the research focused on optimizing the geometric configuration of components which contributes to using the material resources effectively. The problem represents a hybrid solution space containing both continuous and discrete variables (described in more detail in section 4.2). Drawing a comparison to real-world problems, the modular nature of mass-timber projects especially taxes discrete optimization techniques. As Canada continues to see an increase in prefabricated timber structures, the conclusions drawn in this paper from a small structure grow to have broad implications for Canadian cities. The goal of this section is to understand how long it takes to get a reasonably efficient design option with SA versus GA.

## 4.2 How the Component works

<img src="./readme-images/example-working-code.png" width=700>

*Figure 2. Grasshopper workspace*

The main geometric calculations and cost function were written in `C#` as a complied Grasshopper component (seen in Figure 2 as the large node in the middle). This implementation allows Grasshopper's built-in Galapagos component, which implements GA and SA search algorithms, to map the input genes to the cost function and solve for the lowest cost solution (top right magenta node Figure 2). The main user input is a list of 12 column lengths assumed to be extra mass timber columns like the original project.[^7] 

Quickly reviewing the different project parameters, they fall into three broad categories: Project variables which define the basic form of the geometry; gene variables which can be adjusted by the solvers to locate the global minimum; and cost function variables which bias certain parameters to direct evolution toward high-value design options.

Gene variables:  
- SeedA and SeedB control the sequence of columns lengths. It is a discrete variable that has a large impact on the cost function and therefore final design output.  
- Circle Radius controls the overall angle of the timber column off vertical (400 to 900mm).  
- Circle Parameter controls where the timber column starts on the above circle.  
- Front-to-Back and Side-to-Side are continuous variables constrained to give buildable design options that can be modified to maximize timber use and reduce waste (± 11º).  
- Roof Height Offset is a similar continuous variable to above and can be used by the solver to fine tune the fit to the roof plane to the column tops (± 500mm).  

Project variables:  
- User inputted list of 12 column lengths. 
- Depth of mid-tier platform (5000mm used in all experiments).
- Roof ratio multiplier of platform dimensions projected onto plane above (1.4 used throughout). 

Cost function variables:  
- Waste Modifier is a multiplier which penalizes the design when column lengths do not reach the roof plane (set at 3.6).
- Waste Penalty penalizes design options whose columns do not intersect the roof plane, interfere with each other, or do not have adequate bearing under the roof. 

Once the model computes the geometry from the input variables, the cost function determines how wasteful the design option is by calculating how much the columns are above or below the roof plane. Design options are penalized if the columns do not intersect the roof, cross too close to each other mid-span, or do not have adequate bearing under the roof (too close to the edge or other columns). Otherwise, the default settings for Galapagos were used with the exception of modifying the GA's Inbreeding and SA's Drift Rate parameters to improve performance when stuck in local minima.[^8] 

24 sets of 12 columns lengths were each run once using the SA and GA Galapagos solver giving a total of 48 experimental results. In Grasshopper, the component was set to stop and return the results when the solver reached a minimum cost threshold of 2900. The use of a minimal threshold follows from what Simon Herbert called a satisficing solution when searching very large design spaces (Herbert 1969). In problem solving, a consequence of such large design spaces means that finding even one configuration that works may be challenging. Developing a collection of potentially low cost solutions allows designers to efficiently carry out further analysis on a smaller number of candidate designs.

<div>
<img src="./readme-images/gif/runningSolver.gif" width=400>
<img src="./readme-images/gif/SAExample1.gif" width=400>
</div>

*The above animations show the solver in action. The left showing the code executing in Grasshopper, the right displaying the output in Rhino*

<img src="./readme-images/sample-render3.png" width=700>

*Sample SA design option after 388 iterations of cost 2898.59*

<img src="./readme-images/sample-render4.png" width=700>

*Sample GA design option after 3700 iterations of cost 2895.69*

# 5 Preliminary Results
In this section, first some of the analytical results are discussed before turning to the convergence properties of each algorithm. Finally the processed and aggregated data is presented. 

## 5.1 Design Options 
In practice the 2900 cost threshold worked well, and the design options returned were generally buildable, showing a good degree of visual randomness. Normally the design options had a slight pitch to the roof which is helpful in real world applications to shed rain water. The most easily discernible characteristics of high cost design options were column tops not intersecting the roof and excess column length above the roof plane. Also prevalent in high cost solutions were unbuildable intersecting column conditions and columns too short to be realistic. The following table explores some of these high and low cost design options visually in the analytical form seen by the model's cost function.

| High Cost High Waste | Low Cost Low Waste | 
|:-------------------------:|:-------------------------:|
| <img src="./readme-images/table/00-GA-seed01-iter508-5092.15.png" width=400>   Cost 5092.15   | <img src="./readme-images/table/01-GA-seed06-iter703-2603.699.png" width=400>   Cost 2603.70  |
| <img src="./readme-images/table/02-GA-seed07-iter1821-5007.45.png" width=400> Cost 5007.45 | <img src="./readme-images/table/04-SA-seed12-iter455-2889.92.png" width=400> Cost 2889.92 |
| <img src="./readme-images/table/03-SA-seed06-iter212-5882.41.png" width=400> Cost 5882.41 | <img src="./readme-images/table/05-SA-seed24-iter207-2855.98.png" width=400> Cost 2855.98 |


## 5.2 Timing
The full results (Table 4) do not show such clear dominance of SA in computational efficiency or cost effectiveness as seen in the TSP in section 2. The results are more mixed. 

<img src="./readme-images/mm/mm-Table-Results.png" width=600>

*Table 4*

Borrowing from Waibel 2018's framework, the move operator and selection operation had a substantial impact on the performance of the search algorithms which can be seen in the timing results. The SA algorithm uses a population of one, and thereafter keeps track of the best design option so far as it methodically iterates through configurations. Here the temperature parameter of the algorithm is probabilistically defining where to search next across the phase space. Over time the behaviour transitions from jumping far across the design space to marginally modifying the gene variables to search for a better low cost solution locally. The GA on the other hand evaluated the entire population of 50, hitting the solver 50 times before making any changes to the gene variables in the next generation. (This behaviour can be seen in the GA iteration results which stop in intervals of 50.) One might assume then that the GA solver must hit the model 50 times for every one iteration of the SA algorithm. However, as Table 5 shows, this was not the case experimentally. This could be evidence of GA's superior ability to find optional solutions using a population heuristic to search the design space more efficiently.  
 
<img src="./readme-images/mm/mm-Table-Ratios.png" width=250>

*Table 5*

## 5.3 Convergence
In this section, runs were averaged over 25 iterations to highlight the convergence properties of the algorithms. The behaviour identified in the plots had similarities to watching the model go through design iterations in the Rhino workspace. The decreasing step pattern seen in the cost function plots show how updating the combination of columns had a substantial impact on the cost function before the search algorithm transitions to making fine adjustments through changes in the roof and column variables. Also identified in the SA plots is the SA algorithm resetting the temperature parameter approximately every 1500 iterations if a satisfactory solution hasn't been found (set 9 and 22). This is a function of the temperature decay rate. This heuristic makes the SA algorithm more robust to not getting stuck in local minimums. 

<img src="./readme-images/mm/mm-costovertime-cols.png" width=700>

*Table 6*

Figure 3 and 4 show all 24 runs for each solver overlaid. The gaps noticed between the plotted line and cost threshold is an artifact of the averaging process. Numerically, the solve always reaches the threshold and then immediately stops. Averaging over 25 iterations obscures this transition. Figure 3 shows SA on average leveling out somewhere before 500 iterations but the GA (Figure 4) generally starts to level out only after 500 iterations. Both solvers required further iterations to find a satisfactory design option below the cost threshold.  

<img src="./readme-images/mm/mm-costovertime-SA.png" width=700>

<img src="./readme-images/mm/mm-costovertime-GA.png" width=700>

*Figure 3 (top) Figure 4 (bottom)*

## 5.4 Aggregated Results
In this section, before calculating the averages, the two shortest and two longest runs by iteration were removed from each solver's dataset to reduce any artifacts introduced by the algorithm 1) finding an satisficing option in only a few iterations by luck or 2) getting stuck in a local minimum for an extended period of time without finding a better column combination. The majority of the data is included and should be predictive of real-world application.

Plotting the resulting dataset, SA is seen clustering under 500 iterations. The GA results sit above the SA results. As expected, an overall linear relationship is recognized between time and iterations for the whole dataset. (In Figure 5, smaller circles indicate a smaller amount of cost savings under the cost threshold before stopping.)

<img src="./readme-images/mm/mm-Plot-results.png" width=500>

*Figure 5*

Summarizing Table 7, on average, SA could find a satisfactory design option below the cost threshold in approximately half as many iterations as the GA solver. Conversely, even with twice as many iterations, the GA only ever returned slightly more cost-effective configurations. 

<img src="./readme-images/mm/mm-Table-Averages.png" width=600>

*Table 7*

# 6 Conclusions

## 6.1 Summary
Despite the differences in structure between the traveling salesman problem and mass-timber optimization explored in this paper, one conclusion bears highlighting: The relative relationship between methods when applied to discrete optimization problems stays approximately the same between types. Per Table 3 and 7, simulated annealing can generally find a cost-effective solution in less time, and while genetic algorithms are capable of finding better solutions than simulated annealing, they may not be significantly better. It will depend on the unique circumstances of any given project to judge whether the trade off between computational efficiency and robustness is worth the resources.

## 6.1 Future work
There are three main ways of extending this research. Firstly, it was recognized during the project how significant an impact the cost function can make in guiding evolution toward high-value design options. If more attention is given to crafting a cost function which outputs a smoother, more continuous, fitness landscape, it is possible this will make it easier for the model to find design options of higher value more quickly. Secondly, along with the code already completed, it would only take a minimal amount of extra effort to write the GA and SA algorithms directly in `C#` as well. The benefits of this would be an increase in the speed of evaluation and more control over the implementation of the algorithms. This latter point is especially relevant since it's likely possible to better the Galapagos component with a custom implementation of the algorithms to increase robustness. Lastly – speaking to the practicality of such research – it would be valuable to extend this study to include the engineering calculations as well as the geometric computations when optimizing the structure to see if the results shown in this paper hold as complexity increases.

## Acknowledgements
This project was completed as part of the University of Victoria's Faculty of Engineering CIVE 503 class, and is supported by the Energy in Cities group at the University of Victoria. It is for educational purposes only. No code from the original Tianfu project was used or seen by the author of this paper, and we wish to thank StructureCraft for their hard work and dedication in making such a beautiful observation tower. The author also wishes to thank Santiago Diaz for his feedback on the `C#` code included in this project. 

## References
Papers of note reviewed for this project.

Čorić, R., Ðumić, M., Jakobović, D. Genetic programming hyperheuristic parameter configuration using fitness landscape analysis.. Applied Intelligence volume 51 (2021). http://doi.org/10.1007/s10489-021-02227-3  

Deb, K., Pratap, A., Agarwal, S., Meyarivan T. A fast and elitist multiobjective genetic algorithm: NSGA-II. IEEE Transactions on Evolutionary Computation. IEEE Transactions on Evolutionary Computation (2002): 182–97. http://doi.org/10.1109/4235.996017  

Debney, Peter. Computational Engineering. The Insituation of Structural Engineering (2020).  

Evins, Ralph. A review of computational optimisation methods applied to sustainable building design. Renewable and Sustainable Energy Reviews. Volume 22, June 2013, Pages 230-245. https://doi.org/10.1016/j.rser.2013.02.004  

Goldberg, D. E. Genetic algorithms in search, optimization, and machine learning. 1st ed. Addison-Wesley Professional (1989).  

Higham, N. J., Editor. The Princeton Companion to Applied Mathematics. Princeton University Press (2015)

Kirkpatrick S, Gelatt CD, Vecchi MP. Optimization by simulated annealing. Science Volume 37, Issue 6, Pages 853-992 (1983). http://doi.org/10.1287/opre.37.6.865  

Li, Li. The optimization of architectural shape based on Genetic Algorithm. Frontiers of Architectural Research. Volume 1, Issue 4, Pages 392-399 (2012). https://doi.org/10.1016/j.foar.2012.07.005

Rutten, D. Galapagos: On the Logic and Limitations of Generic Solvers. Architectural Design Volune 83, Issue 3,Pages 132-135 (2013). http://dx.doi.org/10.1002/ad.1568

Rutten, D. Navigating Multi-Dimensional landscapes in foggy weather as an analogy for generic problem solving. 16th International Conference on Geometry and Graphics (2014). 

Tan, Z., Li, K., Wang, Y. Differential evolution with adaptive mutation strategy based on fitness landscape analysis. Information Sciences, Volume 549, Pages 142-163 (2021). http://doi.org/10.1016/j.ins.2020.11.023 

Waibel, C. Simulation-based optimization of Buildings and Multi-energy systems	ETH Zurich dissertation no. 25399 (2018).

Wortmann, T., Waibel, C., Nannicini, G., Evins, R., Schroepfer, T., Carmeliet, J. Are genetic algorithms really the best choice for building energy optimization? SIMAUD '17. Proceedings of the Symposium on Simulation for Architecture and Urban Design, Article No.: 6, Pages 1–8 (2017). 

Wortmann, T., Fischer, T. Does architectural design optimization require multiple objectives? A critical analysis. Conference: CAADRIA 2020. 


## Footnotes

[^1]: Both Robert McNeel & Associates's Rhino 7 modelling software and Autodesk's Dynamo have the ability to implement GA and SA optimization in projects.

[^2]: Project's public git repo: https://gitlab.com/wower/csharp-eastudy.git. TSP notebook can be viewed at: https://www.wolframcloud.com/obj/wower/Published/EA-Compare-Algos-OnlinePub.nb as of December 11th 2021.

[^3]: Wolfram Research (2007), FindShortestTour, Wolfram Language function, https://reference.wolfram.com/language/ref/FindShortestTour.html (updated 2015).

[^4]: Presentation by Lucas Epp at CanBIM's Executive Summit on Timber. September 14 2021.

[^5]: The columns were extra mass-timber from another local project and it was important to use as much of it as possible without cutting away too much. 

[^6]: To use this file on Mac OSX, place the compiled binary from the project's repository in the following folder:  
/Users/[yourname]/Library/Application Support/McNeel/Rhinoceros/7.0/Plug-ins/Grasshopper ([yourGUID])/Libraries

[^7]: Here the lengths of columns are represented by 24 CSV files of 12 lengths each so that each run can be used once for each GA and SA. The files of column lengths were created randomly in Mathematica 12.3 and can be found in the project's gitlab repository. 12 numbers between 5500 and 6500 were drawn at random and rounded to nearest interval of 10. 

[^8]: The experiments were run on a 2011 iMac with a 4 GHz Quad-Core Intel i7 CPU and 32 GB 1600 MHz DDR3 RAM. The operating system was MacOS Big Sur 11.5.2. The software was Robert McNeel & Associates Rhino 7.
