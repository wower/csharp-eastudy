﻿using System;
using System.Collections.Generic;

using Rhino.Geometry;
using Rhino.Collections;
using Rhino.NodeInCode;

using static EAStudy.TemplateLibrary;

namespace EAStudy
{
    public class RenderEngine
    {
        public static DesignTemplate design = null;
        public static SetupTemplate setup = null;
        public static RhinoList<Mesh> render_ouput = new RhinoList<Mesh>();

        public RenderEngine(ref SetupTemplate input_setup, ref DesignTemplate input_design)
        {
            setup = input_setup;
            design = input_design;
        }

        private static List<Mesh> RenderColumnMesh()
        {
            // Creates outline of column
            List<Point3d> list_of_initial_pts = new List<Point3d>()
            {
                new Point3d(- setup.ColumnRadius, - setup.ColumnRadius,0),
                new Point3d(- setup.ColumnRadius, setup.ColumnRadius,0),
                new Point3d(setup.ColumnRadius, setup.ColumnRadius,0),
                new Point3d(setup.ColumnRadius, - setup.ColumnRadius,0),
                new Point3d(- setup.ColumnRadius, - setup.ColumnRadius,0),
            };
            Polyline column_outline = new Polyline(list_of_initial_pts);

            //Find Intercept points
            ComponentFunctionInfo GetColumnTops = Components.FindComponent("Line|Plane");

            object[] parm = { design.Columns, design.Roof };
            object[] raw = GetColumnTops.Invoke(parm);
            IList<object> top_pts = (IList<object>)raw[0];

            // Creates Curve from foundation to bottom of roof
            List<Curve> render_curves = new List<Curve>();
            List<Vector3d> trans = new List<Vector3d>();
            for (int i = 0; i < design.Columns.Count; i++)
            {
                Line l = new Line(design.Columns[i].From, (Point3d)top_pts[i]);
                l.Extend(0, 10);
                render_curves.Add(l.ToNurbsCurve());
                trans.Add( new Vector3d(   (Vector3d)design.Columns[i].From - (Vector3d)column_outline.CenterPoint())   );
            }

            // Create Sweep Geometry
            List<Mesh> col = new List<Mesh>();
            for (int i = 0; i < design.Columns.Count; i++)
            {
                Curve shape_to_sweep = column_outline.ToPolylineCurve();
                shape_to_sweep.Translate(trans[i]);
                Brep shape = Brep.CreateFromSweep(
                        render_curves[i],
                        shape_to_sweep,
                        true,
                        0.01
                    )[0];

                // Create Geometry
                Mesh[] meshes = Mesh.CreateFromBrep(shape, MeshingParameters.QualityRenderMesh);
                Mesh mesh = new Mesh();
                foreach (var obj in meshes)
                {
                    mesh.Append(obj);
                }

                // Add to list
                col.Add(mesh);
            }

            return col;
        }

        private static List<Mesh> RenderRoofMesh()
        {
            Brep shape = Brep.CreateFromOffsetFace(design.Roof.Faces[0], 100.0, 0.1, false, true);
            Mesh[] meshes = Mesh.CreateFromBrep(shape, MeshingParameters.QualityRenderMesh);
            Mesh mesh = new Mesh();
            foreach (var obj in meshes)
            {
                mesh.Append(obj);
            }

            List<Mesh> output_geometry = new List<Mesh>() { mesh };

            return output_geometry;
        }

        private static List<Mesh> RenderMidTierMesh()
        {
            Brep shape = Brep.CreateFromOffsetFace(design.MidTierPlane.ToBrep().Faces[0], 120, 0.1, false, true);
            Mesh[] meshes = Mesh.CreateFromBrep(shape, MeshingParameters.QualityRenderMesh);
            Mesh mesh = new Mesh();
            foreach (var obj in meshes)
            {
                mesh.Append(obj);
            }

            List<Mesh> output_geometry = new List<Mesh>() { mesh };

            return output_geometry;
        }

        private static Vector3d FindLineofSymmetry()
        {
            Curve[] ol = design.MidTierPlane.ToBrep().GetWireframe(0);

            double avg = 0;
            foreach (Curve c in ol)
            {
                avg += c.GetLength();
            }
            avg = avg / 4;

            List<Curve> longer = new List<Curve>();
            foreach (Curve c in ol)
            {
                if (c.GetLength() > avg)
                {
                    longer.Add(c);
                }
            }

            return new Vector3d(longer[0].PointAtNormalizedLength(0.5));
        }


        private static Mesh DeliverFoundationVolumn(Point3dList pts, double depth)
        {
            BoundingBox bb = pts.BoundingBox;
            bb.Inflate(100, 100, depth/2);

            Brep obj = bb.ToBrep();
            obj.Translate(new Vector3d(0.0, 0.0, -depth/2));

            Mesh[] box = Mesh.CreateFromBrep(obj, MeshingParameters.QualityRenderMesh);
            Mesh foundation = new Mesh();
            foreach (var item in box)
            {
                foundation.Append(item);
            }

            return foundation;
        }

        private static List<Mesh> RenderFoundation()
        {
            Vector3d axis_of_symmmetry = FindLineofSymmetry();
            Point3dList right_pts = new Point3dList();
            Point3dList left_pts = new Point3dList();

            // Sort left and right pts to get extends
            for (int i = 0; i < design.Columns.Count; i++)
            {
                Point3d test_pt = design.Columns[i].From;

                if (test_pt.Y < axis_of_symmmetry.Y)
                {
                    right_pts.Add(test_pt);
                }
                else if (test_pt.Y > axis_of_symmmetry.Y)
                {
                    left_pts.Add(test_pt);
                }
            }

            return new List<Mesh>() {
                DeliverFoundationVolumn(right_pts, 1000),
                DeliverFoundationVolumn(left_pts, 1000)
            };

        }

        public RhinoList<Mesh> Main_Rendering_Function()
        {
            render_ouput.Clear();

            // Create closed columns
            render_ouput.AddRange(RenderColumnMesh());

            // Create roof plane
            render_ouput.AddRange(RenderRoofMesh());

            // Create mid tier
            render_ouput.AddRange(RenderMidTierMesh());

            // Create foundation
            render_ouput.AddRange(RenderFoundation());

            return render_ouput;
        }
    }
}
