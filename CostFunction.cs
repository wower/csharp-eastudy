﻿using System;
using System.Collections.Generic;

using Rhino.Geometry;
using Rhino.NodeInCode;

using static EAStudy.TemplateLibrary;

namespace EAStudy
{
    public class CostFunction
    {
        public static SetupTemplate setup = null;
        public static DesignTemplate design = null;
        public static RESULTS results = new RESULTS();

        public CostFunction(ref SetupTemplate input_setup, ref DesignTemplate input_design)
        {
            setup = input_setup;
            design = input_design;
        }

        private static bool CheckColumnRoofIntercept()
        {
            design.roof_pts = ToolLibrary.FindRoofPoints(design);
            // Returns true if penalty needs to be applied because columns do not intercept roof plane
            return design.roof_pts.Count != design.Columns.Count;
        }

        private static bool CheckForColumnConflicts()
        {
            // Returns true if penalty needs to be applied for column clash ///
            // Make Pipes from column lines
            List<Mesh> pipes = new List<Mesh>();
            for (int i = 0; i < design.Columns.Count; i++)
            {
                // Create pipe shape
                Brep shape = Brep.CreatePipe(
                        design.Columns[i].ToNurbsCurve(),
                        setup.ColumnRadiusTolerence,
                        false,
                        PipeCapMode.Flat,
                        true,
                        0.01,
                        0.01
                    )[0];

                // Create Geometry
                Mesh[] meshes = Mesh.CreateFromBrep(shape, MeshingParameters.QualityRenderMesh);
                Mesh mesh = new Mesh();
                foreach (var obj in meshes)
                {
                    mesh.Append(obj);
                }

                // Add to list
                pipes.Add(mesh);
            }

            // Excutes search for column clashes within column set up column tolerence
            Rhino.Geometry.Intersect.MeshClash[] clash = Rhino.Geometry.Intersect.MeshClash.Search(
                pipes,
                pipes,
                0.001,
                99
                );

            // Gets clash points and drops lower ones. 
            List<Point3d> clashpoints = new List<Point3d>();
            foreach (Rhino.Geometry.Intersect.MeshClash item in clash)
            {
                if(item.ClashPoint.Z > 100)
                {
                    clashpoints.Add(item.ClashPoint);
                }
            }

            return clashpoints.Count != 0;
        }

        private static bool CheckForRoofConflicts()
        {
            // Returns true if penalty needs to be applied for columns being too close on the roof

            // convert to get roof brep to plane
            List<Point3d> points_in_plane = new List<Point3d>();
            for (int i = 0; i < (design.Roof.Vertices.Count - 1); i++)
            {
                points_in_plane.Add(design.Roof.Vertices[i].Location);
            }
            Plane pl = new Plane(points_in_plane[0], points_in_plane[1], points_in_plane[2]);

            // Create Circles
            List<Mesh> circles = new List<Mesh>();
            for (int i = 0; i < design.roof_pts.Count; i++)
            {
                Brep c = new Sphere(design.roof_pts[i], setup.ColumnSeatingTolerence).ToBrep();
                Mesh[] meshes = Mesh.CreateFromBrep(c, MeshingParameters.Default);
                Mesh mesh = new Mesh();
                foreach (var obj in meshes)
                {
                    mesh.Append(obj);
                }
                circles.Add(mesh);
            }

            // Check for Curve Clashes
            List<Point3d> clash_pts = new List<Point3d>();
            Rhino.Geometry.Intersect.MeshClash[] clash = Rhino.Geometry.Intersect.MeshClash.Search(
                circles,
                circles,
                0.001,
                99
                );

            foreach (Rhino.Geometry.Intersect.MeshClash item in clash)
            {
                clash_pts.Add(item.ClashPoint);
            }


            // Find closet roof pts to outline brep with roof tolerence
            PolyCurve roof_outline = new PolyCurve();
            foreach (var item in design.Roof.Edges)
            {
                roof_outline.Append(item);
            }

            List<Point3d> closest_pts = new List<Point3d>();
            for (int i = 0; i < design.roof_pts.Count; i++)
            {
                double _ = 0.1;
                if (roof_outline.ClosestPoint(design.roof_pts[i], out _, setup.ColumnSeatingTolerence))
                {
                    closest_pts.Add(design.roof_pts[i]);
                };
            }
            
            // Output logic: Return true if CheckColumnRoofIntercept is true
            if (results.roof_intercept) // True
            {
                return true;
            } else if (results.roof_intercept) // False
            {
                return clash_pts.Count != 12 | closest_pts.Count != 0;
            } else
            {
                return false; // Safety Catch
            }
        }

        private static List<WasteVector> CalculateWaste()
        {

            // Get Ends points
            List<Point3d> end_pts = new List<Point3d>();
            foreach (Line item in design.Columns)
            {
                end_pts.Add(item.To);
            } 

            // Create Vectors and get z direction
            List<WasteVector> vectors = new List<WasteVector>();

            List<Vector3d> debugging_vectors = new List<Vector3d>();
            List<Point3d> debugging_pts = new List<Point3d>();

            for (int i = 0; i < design.roof_pts.Count; i++)
            {
                Vector3d vec = new Vector3d((Vector3d)end_pts[i]-(Vector3d)design.roof_pts[i]);
                debugging_vectors.Add(vec);
                debugging_pts.Add(design.roof_pts[i]);
                vectors.Add(new WasteVector(vec.Length, vec.Z));
            }

            return vectors;
        }

        private static double MassAddition()
        {
            double cost = 0;

            for (int i = 0; i < results.waste.Count; i++){
                if (results.waste[i].Direction < 0)
                {
                    cost += results.waste[i].Length * setup.waste_modifier;
                } else if (results.waste[i].Direction >= 0)
                {
                    cost += results.waste[i].Length;
                }
            }

            if (results.roof_intercept)
            {
                cost += setup.waste_penatly;
            }

            if (results.column_check)
            {
                cost += setup.waste_penatly;
            }

            if (results.roof_check)
            {
                cost += setup.waste_penatly;
            }

            return cost;
        }

        public double Main_Cost_Function()
        {
            // Check if should add penalty for column intercepting roof plane
            results.roof_intercept = CheckColumnRoofIntercept();

            // Check if should add penalty for colunm conflicts
            results.column_check = CheckForColumnConflicts();

            // Check if should add penalty for column seating at roof plane
            results.roof_check = CheckForRoofConflicts();

            results.waste = CalculateWaste();

            return MassAddition();
        }
    }
}
