using System;

using Rhino.Geometry;
using Rhino.Collections;


using static EAStudy.TemplateLibrary;
using Grasshopper.Kernel;

namespace EAStudy
{
    public class EAStudyGeometry : GH_Component
    {
        public EAStudyGeometry()
          : base("EAStudy Geometry Component", "Geometry Engine",
            "Creates Geometry and Calculates Cost Function",
            "EA Study Components", "Algorithm Component")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBooleanParameter("Toogle", "T", "Run Script.", GH_ParamAccess.item);
            pManager.AddBooleanParameter("Render", "T", "Return Script.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Column Lengths", "L", "List of Column Lengths.", GH_ParamAccess.list);
            pManager.AddIntegerParameter("Mid Teir Height", "H", "Height of Mid Teir.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Mid Tier Depth", "D", "Number of Division.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Roof Scale", "S", "Scale of roof from Mid Tier.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Gene SeedA", "A", "Sequence of Columns.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Gene SeedB", "B", "Sequence of Columns.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Gene Circle Radius", "R", "Radius of Circlc.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Gene Parameter on Circle", "C", "Point on Circle by Curve Parameter.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Gene Roof Side-to-Side Angle", "S", "Angle in Radians.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Gene Roof Front-to-Back Angle", "F", "Angle in Radians.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Gene Roof Height Offset", "E", "Z Axis Offset in mm.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Waste penatly", "P", "penatly for cost function.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Waste Modifier", "M", "Cost Function modifier for columns.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Roof", "R", "Roof Geometry Output", GH_ParamAccess.item);
            pManager.AddGenericParameter("Columns", "C", "Column Axis", GH_ParamAccess.list);
            pManager.AddGenericParameter("MidTier", "M", "MidTier Geometry Output", GH_ParamAccess.item);
            pManager.AddNumberParameter("Cost", "$", "Cost of Structure", GH_ParamAccess.item);
            pManager.AddTextParameter("Record", "R", "Time and Cost Array", GH_ParamAccess.item);
            pManager.AddGenericParameter("Render", "D", "Render Geometry Output", GH_ParamAccess.list);
        }

        /// Solver
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Declare Solver Variables
            bool run_toggle = false;
            bool geometry_toggle = false;
            double cost = 0.0;
            string record = "Nothing to Return";
            RhinoList<Mesh> render = new RhinoList<Mesh>();
            SetupTemplate setup = new SetupTemplate(DA);
            DesignTemplate design = null;

            // Use the DA object to retrieve the data inside the first input parameter.
            if (!DA.GetData(0, ref run_toggle)) { return; }
            if (!DA.GetData(1, ref geometry_toggle)) { return; }

            // Execute Main Functions
            if (run_toggle is true)
            {
                // Create Test Build
                GeometryEngine ge = new GeometryEngine(ref setup);
                design = ge.Main_Geometry_Function();

                // Calculate Cost
                CostFunction cf = new CostFunction(ref setup, ref design);
                cost = cf.Main_Cost_Function();
                record = ToolLibrary.RecordsEngine(cost);

                //Execute Rendered output
                if (geometry_toggle is true)
                {
                    RenderEngine re = new RenderEngine(ref setup, ref design);
                    render = re.Main_Rendering_Function();
                }
                else if (geometry_toggle is false)
                {
                    // Do Nothing
                    render.Clear();
                }
            } else
            {
                design = new DesignTemplate();
                render = new RhinoList<Mesh>();
            }

            // Use the DA object to assign a new String to the first output parameter.
            DA.SetData(0, design.Roof);
            DA.SetDataList(1, design.Columns);
            DA.SetData(2, design.MidTierPlane);
            DA.SetData(3, cost);
            DA.SetData(4, record);
            DA.SetDataList(5, render); 
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.Resources.icon1;
            }
        }

        public override Guid ComponentGuid => new Guid("5E801807-1FA8-48C7-A9A7-3275F8A0F33D");
    }
}