﻿using System;
using System.Drawing;
using Grasshopper;
using Grasshopper.Kernel;

namespace EAStudy
{
    public class EAStudyInfo : GH_AssemblyInfo
    {
        public override string Name => "EAStudyGeometry";

        //Return a 24x24 pixel bitmap to represent this GHA library.
        public override Bitmap Icon => null;

        //Return a short string describing the purpose of this GHA library.
        public override string Description => "";

        public override Guid Id => new Guid("6D670CE8-012F-4120-BDC2-4A0DD4A66A98");

        //Return a string identifying you or your company.
        public override string AuthorName => "";

        //Return a string representing your preferred contact details.
        public override string AuthorContact => "";
    }
}