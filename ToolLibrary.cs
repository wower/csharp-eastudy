﻿using System;
using System.Collections.Generic;
using Rhino.Geometry;
using Rhino.Collections;
using Rhino.NodeInCode;

using static EAStudy.TemplateLibrary;

namespace EAStudy
{
    public class ToolLibrary
    {
        public static List<int> DeveloperVariables()
        // For Dubugging 
        {
            return new List<int>() { 6010, 6020, 6030, 6040, 6050, 6060, 6070, 6080, 6090, 6120, 6110, 6120 };
        }

        public static List<double> DeveloperCircleRadius()
        {
            // Developer Debugging Helper Function
            return new List<double>(12) { 600.0, 1100.0, 500.0, 700.0, 800.0, 1010.0, 630.0, 900.0, 830.0, 900.0, 1060.0, 740.0 };
        }

        public static List<double> DeveloperCircleParameter()
        {
            // Developer Debugging Helper Function
            List<double> rand = new List<double>();
            Random func = new Random();

            for (int i = 0; i < 12; i++)
            {
                rand.Add(func.NextDouble());
            }
            return rand;
        }

        public static void GetDelegateInfo(ComponentFunctionInfo func)
        {
            // For Dubugging
            // Prints information about grasshopper node

            Console.WriteLine($"\nFunction: {func.FullName}");
            Console.WriteLine($"Description: {func.Description}");

            for (int i = 0; i < func.InputNames.Count; i++)
            {
                Console.WriteLine($"\nName: {func.InputNames[i]} ");
                Console.WriteLine($"Type: {func.InputTypeNames[i]} ");
                Console.WriteLine($"Optional: {func.InputsOptional[i]} ");
            }
            for (int i = 0; i < func.OutputNames.Count; i++)
            {
                Console.WriteLine($"\nOutput: {func.OutputNames[i]} ");
                Console.WriteLine($"Types: {func.OutputTypeNames[i]} ");
            }

        }

        public static List<int> ObjToList(IList<object> objects)
        {
            // Converts lists of python objects to list of int 
            List<int> intList = new List<int>();
            foreach (object o in objects)
            {
                intList.Add((int)o);
            }
            return intList;
        }

        public static Point3dList ProcessCurves(ComponentFunctionInfo func, Curve c, int seed)
        {
            // Helper function to reduce duplicate work in GetRandomIntercepts method
            object[] p = { c, 6, seed };

            object[] raw = func.Invoke(p);

            IList<object> items = (IList<object>)raw[0];

            Point3dList pts = new Point3dList();

            foreach (object item in items)
            {
                pts.Add((Point3d)item);
            }

            return pts;
        }

        public static Vector3d GetAxisOfRotation(List<Curve> l)
        {
            Point3d midA = l[0].PointAtNormalizedLength(0.5);
            Point3d midB = l[1].PointAtNormalizedLength(0.5);

            Vector3d vec = Vector3d.Subtract((Vector3d)midA, (Vector3d)midB);

            return vec;
        }

        public static List<Point3d> FindRoofPoints(DesignTemplate input)
        {
            ComponentFunctionInfo BrepLineIntersect = Components.FindComponent("Brep|Line");

            object[] parm = { input.Roof, input.Columns };
            object[] raw = BrepLineIntersect.Invoke(parm);
            IList<object> raw_pts = (IList<object>)raw[1];
            List<Point3d> final_pts = new List<Point3d>();
            foreach (var obj in raw_pts)
            {
                final_pts.Add((Point3d)obj);
            }

            return final_pts;
        }

        public static string RecordsEngine(double cost)
        {
            // Formats output JSON string
            DateTime t = DateTime.Now;

            string output_JSON_string = $" {{cost -> {cost}, time-> {t.TimeOfDay}}} ";

            return output_JSON_string;
        }
    }
}
