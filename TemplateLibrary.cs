﻿using System;
using System.Collections.Generic;

using Rhino.Geometry;
using Rhino.Collections;

using Grasshopper.Kernel;

namespace EAStudy
{
    public class TemplateLibrary
    {
        public class SetupTemplate
        {
            // Dummy Variables for debugging
            //public List<int> ColumnLengths = ToolLibrary.DeveloperVariables();
            //public List<double> CircleRadius = ToolLibrary.DeveloperCircleRadius();
            //public List<double> CircleParameter = ToolLibrary.DeveloperCircleParameter();


            /// Declare Variables for user inputs ///
            public List<int> ColumnLengths = new List<int>();
            public List<double> CircleRadius = new List<double>();
            public List<double> CircleParameter = new List<double>();
            public int MidTierHeight = 2510;
            public int MidTierDepth = 3610;
            public double RoofScale = 1.45;
            public int seedA = 3913;
            public int seedB = 5430;
            public double side_to_side_radians = 0.1;
            public double front_to_back_radians = 0.1;
            public int RoofHeight = 0;
            public int waste_penatly = 950;
            public double waste_modifier = 3.0;

            /// Geometry Parameters ///
            public double golden = 1.61803398874989484820458683436;
            public int RandomLeftEdgeSeed = 0; // Random Point seed on mid Tier
            public int RandomRightEdgeSeed = 1; // Random Point seed on mid Tier
            public double ColumnRadius = 30; // Radius of timber columns

            /// Cost Function Parameters ///
            public int ColumnRadiusTolerence = 90; // Tolerence of Column Clashes in mm in Cost Function
            public double ColumnSeatingTolerence = 400; // Tolerence of Columns in mm attached to roof plane

            // Constuctor for user input
            public SetupTemplate(IGH_DataAccess DA)
            {
                if (!DA.GetDataList(2, ColumnLengths)) { return; }
                if (!DA.GetData(3, ref MidTierHeight)) { return; }
                if (!DA.GetData(4, ref MidTierDepth)) { return; }
                if (!DA.GetData(5, ref RoofScale)) { return; }
                if (!DA.GetData(6, ref seedA)) { return; }
                if (!DA.GetData(7, ref seedB)) { return; }
                if (!DA.GetDataList(8, CircleRadius)) { return; }
                if (!DA.GetDataList(9, CircleParameter)) { return; }
                if (!DA.GetData(10, ref side_to_side_radians)) { return; }
                if (!DA.GetData(11, ref front_to_back_radians)) { return; }
                if (!DA.GetData(12, ref RoofHeight)) { return; }
                if (!DA.GetData(13, ref waste_penatly)) { return; }
                if (!DA.GetData(14, ref waste_modifier)) { return; }
            }
        }

        public class DesignTemplate
        {
            // Collects output Geometry and data
            public List<int> ColumnSequence = new List<int>();
            public PlaneSurface MidTierPlane = null;
            public Point3dList InterceptPts = null;
            public List<Line> Columns = new List<Line>();
            public Brep Roof = new Brep();
            public List<Point3d> roof_pts = new List<Point3d>();
            public Plane RoofPlane = new Plane();
        }

        public class RESULTS
        {
            public bool roof_intercept { get; set; }
            public bool roof_check { get; set; }
            public bool column_check { get; set; }
            public List<WasteVector> waste { get; set; }
        }

        public class WasteVector
        {
            public double Length { get; set; }
            public double Direction { get; set; }

            public WasteVector(double len, double dir)
            {
                Length = len;
                Direction = dir;
            }
        }

    }
}
