﻿using System;
using System.Collections.Generic;

using Rhino;
using Rhino.Geometry;
using Rhino.Collections;
using Rhino.NodeInCode;

using static EAStudy.TemplateLibrary;

namespace EAStudy
{
    public class GeometryEngine
    {
        private static SetupTemplate setup;

        public GeometryEngine(ref SetupTemplate input)
        {
            // Class Constructor
            setup = input;

        }

        public static List<int> GetColumnSequence()
        {
            int seed = setup.seedA * setup.seedB;

            // Declare Delegate
            ComponentFunctionInfo jitter = Components.FindComponent("Jitter");

            // Execute Function
            object[] args = { setup.ColumnLengths, 1.0, seed };
            object[] raw = jitter.Invoke(args);
            List<int> sequence = ToolLibrary.ObjToList((IList<object>)raw[0]);

            return sequence;
        }

        public static PlaneSurface CreateMidTier()
        {
            var plane_surface = new PlaneSurface(
                Plane.WorldXY,
                new Interval(0, setup.MidTierDepth),
                new Interval(0, setup.MidTierDepth * setup.golden)
                );

            plane_surface.Translate(new Vector3d(0, 0, setup.MidTierHeight));

            return plane_surface;
        }

        public static Point3dList GetRandomIntercepts(PlaneSurface pl)
        {
            // Returns 2 lists of 6 random intercept points on sides of mid tier
            // Get Both short sides
            List<Curve> rawCurves = new List<Curve>(4);
            foreach (BrepEdge edge in pl.ToBrep().Edges)
            {
                rawCurves.Add(edge.EdgeCurve);
            }

            // Check Lengths & get average length
            double avg = 0.0;
            foreach (Curve edge in rawCurves)
            {
                avg += edge.GetLength();
            }

            // Sort critical curves from surface edges
            List<Curve> important_curves = new List<Curve>(2);
            avg /= rawCurves.Count;
            foreach (Curve edge in rawCurves)
            {
                if (edge.GetLength() < avg)
                {
                    important_curves.Add(edge);
                }
            }

            // Apply Geometry Random points to each curve
            // Declare Function
            ComponentFunctionInfo pop = Components.FindComponent("PopulateGeometry");

            Point3dList pts = ToolLibrary.ProcessCurves(pop, important_curves[0], setup.RandomLeftEdgeSeed);
            pts.AddRange(ToolLibrary.ProcessCurves(pop, important_curves[1], setup.RandomRightEdgeSeed));

            return pts;
        }

        public static List<Line> CreateAngledColumns(Point3dList mid_pts, List<int> col_length)
        {
            // create lower point
            List<Point3d> lower_pts = new List<Point3d>();
            for ( int i = 0; i< mid_pts.Count; i++)
            {
                lower_pts.Add(new Point3d(mid_pts[i].X, mid_pts[i].Y, 0));
            }

            // Create Circle at lower point
            List<Circle> circles = new List<Circle>();
            for (int i = 0; i < lower_pts.Count; i++)
            {
                circles.Add(new Circle(
                    lower_pts[i], setup.CircleRadius[i]
                    ));
            }

            // Insubstaniate Function & get tangent points
            ComponentFunctionInfo evalCurve = Components.FindComponent("EvaluateCurve");

            Point3dList tangent_pts = new Point3dList();
            for (int i = 0; i<lower_pts.Count; i++)
            {
                double c = circles[i].Circumference;
                double p = setup.CircleParameter[i];
                object[] parm = { circles[i], p*c };
                object[] raw = evalCurve.Invoke(parm);
                IList<object> items = (IList<object>)raw[0];
                tangent_pts.Add((Point3d)items[0]);
            }

            // Create new Vector3d between tangent point to mid tier intercept point
            // Create Line by Start/Vector/Direction
            List<Line> major_lines = new List<Line>();
            for (int i = 0; i < lower_pts.Count; i++)
            {
                Vector3d vec = Vector3d.Subtract((Vector3d)mid_pts[i], (Vector3d)tangent_pts[i]);
                major_lines.Add(new Line(tangent_pts[i], vec, col_length[i] ));
            }

            return major_lines;
        }

        public DesignTemplate Main_Geometry_Function()
        {
            // Set up
            DesignTemplate design = new DesignTemplate();

            // Create Sequence of Column Lengths
            design.ColumnSequence = GetColumnSequence();

            // Create Mid Teir Plane
            design.MidTierPlane = CreateMidTier();

            // Get Random Intercept Points of Mid Tier Plane
            design.InterceptPts = GetRandomIntercepts(design.MidTierPlane);

            // Create Angled Column Axis
            design.Columns = CreateAngledColumns(design.InterceptPts, design.ColumnSequence);

            // Create & Scale Roof
            RoofEnigine re = new RoofEnigine(ref setup, ref design);
            design.Roof = re.CreateRoof(design.MidTierPlane, design.Columns);

            return design;
        }
    }
}
