﻿using System;
using System.Collections.Generic;

using Rhino.Geometry;
using Rhino.NodeInCode;

using static EAStudy.TemplateLibrary;

namespace EAStudy
{
    public class RoofEnigine
    {
        public static SetupTemplate setup;
        public static DesignTemplate design;

        public RoofEnigine(ref SetupTemplate input_setup, ref DesignTemplate input_design)
        {
            setup = input_setup;
            design = input_design;
        }

        private static List<Line> MidTeirExtents(PlaneSurface mid_tier)
        {
            List<Point3d> corners = new List<Point3d>();
            Rhino.Geometry.Collections.BrepVertexList vert = mid_tier.ToBrep().Vertices;
            for (int i = 0; i < 4; i++)
            {
                corners.Add(vert[i].Location);
            }

            List<Line> extents = new List<Line>();
            double height_of_extents = 12000.0; // Hard Coded
            foreach (Point3d corner in corners)
            {
                extents.Add(
                    new Line(corner, new Point3d(corner.X, corner.Y, height_of_extents))
                    );
            }

            return extents;
        }

        private static void FitPlane(List<Line> cols)
        {
            // Get end pts of columns
            List<Point3d> end_pts = new List<Point3d>();
            for (int i = 0; i < cols.Count; i++)
            {
                end_pts.Add(cols[i].To);
            }

            // Declare planefit function
            ComponentFunctionInfo FitPlaneFunction = Components.FindComponent("PlaneFit");

            // Evaluate end points with GH component "Plane Fit"
            object[] parm = { end_pts };
            object[] raw = FitPlaneFunction.Invoke(parm);
            IList<object> pl = (IList<object>)raw[0];

            design.RoofPlane = (Plane)pl[0];
        }

        private static Brep RoofOutline(List<Line> extents)
        {
            // Evaluate end points with GH component "Plane Fit"
            ComponentFunctionInfo LinePlaneIntersect = Components.FindComponent("Line|Plane");

            object[] parm = { extents, design.RoofPlane };
            object[] raw = LinePlaneIntersect.Invoke(parm);
            IList<object> roof_pl = (IList<object>)raw[0];

            // Brep Surface
            Brep suf = Brep.CreateFromCornerPoints(
                (Point3d)roof_pl[0],
                (Point3d)roof_pl[1],
                (Point3d)roof_pl[2],
                (Point3d)roof_pl[3],
                0.01
                );

            return suf;
        }

        private static Brep TransformRoof(Brep suf)
        {
            Point3d cent = AreaMassProperties.Compute(suf).Centroid;

            // Seperate long and short axis
            Curve[] ol = suf.GetWireframe(0);

            if (ol.Length != 4)
            {
                throw new Exception($"TransformRoof: Curver count is incorrect. Should be 4. Instead {ol.Length}");
            }

            double avg = 0;
            foreach (Curve c in ol)
            {
                avg += c.GetLength();
            }
            avg = avg / 4;

            List<Curve> longer = new List<Curve>();
            List<Curve> shorter = new List<Curve>();
            foreach (Curve c in ol)
            {
                if (c.GetLength() > avg)
                {
                    longer.Add(c);
                }
                else if (c.GetLength() < avg)
                {
                    shorter.Add(c);
                }
            }

            // Get Axis of Rotation
            Vector3d side_to_side_vec = ToolLibrary.GetAxisOfRotation(longer);
            Vector3d front_to_back_vec = ToolLibrary.GetAxisOfRotation(shorter);

            Transform side_to_side_trans = Transform.Rotation(setup.side_to_side_radians, side_to_side_vec, cent);
            Transform front_to_back_trans = Transform.Rotation(setup.front_to_back_radians, front_to_back_vec, cent);

            suf.Transform(side_to_side_trans);
            suf.Transform(front_to_back_trans);

            // Scale Surface from set up parameter
            Transform trans = Transform.Scale(cent, setup.RoofScale); 

            suf.Transform(trans);

            // Adjust Roof Height
            suf.Translate(new Vector3d(0, 0, setup.RoofHeight));

            // Return final surface plane
            return suf;
        }

        public Brep CreateRoof(PlaneSurface mid_tier, List<Line> cols)
        {
            // Extents from mid teir to make roof
            List<Line> extents = MidTeirExtents(mid_tier);

            // Lines to create plane fit for roof.
            FitPlane(cols);

            // Creates Roof outline
            Brep roof = RoofOutline(extents);

            // Translate roof angles and scale
            roof = TransformRoof(roof);

            return roof;
        }
    }
}
